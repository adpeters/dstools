import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib import patches

sequential_palette = sns.color_palette("BuGn")


def get_metric_tables(y_test, y_pred):
    y_test = pd.Series(y_test)

    df = pd.concat([y_test.rename("y_test"), 
                    pd.Series(y_pred, index=y_test.index, name="y_pred")], 
                    axis=1).astype("int64")
    df = (pd.crosstab(df["y_pred"], df["y_test"])
            .sort_values(by="y_pred", ascending=False)[[1, 0]])
    return df


def get_metrics(y_test, y_pred):
    df = get_metric_tables(y_test, y_pred)

    # fill in missing values if there aren't any True/False predictions or actuals
    if 1 not in df.columns:
        df[1] = np.NaN
    if 0 not in df.columns:
        df[0] = np.NaN
    if 1 not in df.index:
        df.loc[1, :] = np.NaN
    if 0 not in df.index:
        df.loc[0, :] = np.NaN

    # calc metrics
    if 1 not in df.columns:
        precision = np.NaN
        recall = np.NaN
        miss_rate = np.NaN
        false_omission_rate = np.NaN
    else:
        precision = df.loc[1, 1] / df.loc[1, :].sum()
        recall = df.loc[1, 1] / df.loc[:, 1].sum()
        miss_rate = df.loc[0, 1] / df.loc[:, 1].sum()
        false_omission_rate = df.loc[0, 1] / df.loc[0, :].sum()

    selectivity = df.loc[0, 0] / df.loc[:, 0].sum()
    npv = df.loc[0, 0] / df.loc[0, :].sum()
    fall_out = df.loc[1, 0] / df.loc[:, 0].sum()
    false_discovery_rate = df.loc[1, 0] / df.loc[1, :].sum()
    return precision, recall, selectivity, npv, miss_rate, fall_out, false_discovery_rate, false_omission_rate

def get_metric_text(metrics=None, y_test=None, y_pred=None):
    if metrics is not None:
        precision, recall, selectivity, npv, miss_rate, fall_out, false_discovery_rate, false_omission_rate = metrics
    else:
        precision, recall, selectivity, npv, miss_rate, fall_out, false_discovery_rate, false_omission_rate = get_metrics(y_test, y_pred)
    # get text of metrics
    format_str = "{:<42s} {:<10.3f} {}\n"
    texts = [
        ("Precision (Positive Predictive Value):", precision, "Out of all our positive predictions, we picked this many right."),
        ("Recall (True Positive Rate):", recall, "Out of all true, we picked this many right."),
        ("Selectivity (True Negative Rate):", selectivity, "Out of all false, we picked this many right."),
        ("Negative Predictive Value:", npv, "Out of all our negative predictions, we picked this many right."),
        ("Miss Rate (False Negative Rate):", miss_rate, "Out of all true, we picked this many wrong."),
        ("Fall-out (False Positive Rate):", fall_out, "Out of all false, we picked this many wrong."),
        ("False Discovery Rate:", false_discovery_rate, "Out of all our positive predictions, we picked this many wrong."),
        ("False Omission Rate", false_omission_rate, "Out of all our negative predictions, we picked this many wrong."),
    ]
    txt = ""
    for text in texts:
        txt += format_str.format(*text)
    return txt
    

def draw_confusion_matrix(y_test, y_pred):
    df = get_metric_tables(y_test, y_pred)
    precision, recall, selectivity, npv, miss_rate, fall_out, false_discovery_rate, false_omission_rate = get_metrics(y_test, y_pred)

    # Draw confusion matrix
    fig, ax = plt.subplots(figsize=(6, 6))
    sns.heatmap(df,
                ax=ax,
                annot=True,
                cbar=False,
                fmt="0.0f",
                linewidths=3,
                cmap=sequential_palette)
    
    ax.xaxis.set_label_position("top")
    ax.xaxis.set_ticks_position("top")
    ax.xaxis.set_label_text("Actual Value")
    ax.yaxis.set_label_text("Predicted Value")
    
    # add metrics to figure
    block_alpha = 0.5
    # Create a Rectangle patch for precision / false discovery rate
    rect = patches.Rectangle((0,0), 1.99, 0.33, linewidth=1, edgecolor='black', facecolor='white', alpha=block_alpha)
    # Add the patch to the Axes
    ax.add_patch(rect)
    # Add text to patch
    ax.text(.5, 0.165, "{:.2f}".format(precision), horizontalalignment="center", verticalalignment="center")
    # Add text to patch
    ax.text(1.5, 0.165, "{:.2f}".format(false_discovery_rate), horizontalalignment="center", verticalalignment="center")
    
    # Create a Rectangle patch for recall / miss-rate
    rect = patches.Rectangle((0,0), 0.33, 1.99, linewidth=1, edgecolor='black', facecolor='white', alpha=block_alpha)
    # Add the patch to the Axes
    ax.add_patch(rect)
    
    # Add text to patch
    ax.text(0.165, .5, "{:.2f}".format(recall), horizontalalignment="center", verticalalignment="center")
    # Add text to patch
    ax.text(0.165, 1.5, "{:.2f}".format(miss_rate), horizontalalignment="center", verticalalignment="center")

    # Create a Rectangle patch for selectivity / fall-out
    rect = patches.Rectangle((1.66,0), 0.33, 1.99, linewidth=1, edgecolor='black', facecolor='white', alpha=block_alpha)
    # Add the patch to the Axes
    ax.add_patch(rect)
    # Add text to patch
    ax.text(2 - 0.165, 1.5, "{:.2f}".format(selectivity), horizontalalignment="center", verticalalignment="center")
    # Add text to patch
    ax.text(2 - 0.165, 0.5, "{:.2f}".format(fall_out), horizontalalignment="center", verticalalignment="center")
    
    # Create a Rectangle patch for npv / false omission rate
    rect = patches.Rectangle((0,1.66), 1.99, 0.33, linewidth=1, edgecolor='black', facecolor='white', alpha=block_alpha)
    # Add the patch to the Axes
    ax.add_patch(rect)
    # Add text to patch
    ax.text(1.5, 2 - 0.165, "{:.2f}".format(npv), horizontalalignment="center", verticalalignment="center")
    # Add text to patch
    ax.text(0.5, 2 - 0.165, "{:.2f}".format(false_omission_rate), horizontalalignment="center", verticalalignment="center")


def generate_heatmap_two_vars(df,
                              var1,
                              var2,
                              title,
                              value="adm_appl_nbr",
                              aggfunc="nunique",
                              colors_by="columns",
                              pct_by="rows",
                              pct_title=None,
                              reverse=False,
                              cumulative_pct=False,
                              **heatmap_kwargs):
    """
    Generate a heatmap for two variables.

    Parameters:
    df = dataframe to use
    var1 = variable on y axis
    var2 = variable on x axis
    value = value to aggregate on in crosstab. default is adm_appl_nbr
    aggfunc = aggregation function for crosstab. default is "nunique"
    title = title of chart
    colors_by = option to have colors mapped by each column or
                each row rather than all values. possible values
                are "all", "columns", or "rows". default is "columns".
    pct_by = option to include a second chart that uses
                      percent of total for each row or column rather
                      than actual values. values are None,
                      "columns", or "rows". default is "rows".
    pct_title = title of percent chart if it's included
    reverse = reverse order of rows or columns or both. possible
              values are False, "rows", "columns", or True. default
              is False.
    cumulative_pct = option to have percents be cumulative instead of
                     individual values. Options are True or False.
                     default is False.
    heatmap_kwargs = kwargs passed to seaborn.heatmap()

    """
    df = df.reset_index()[[value, var1, var2]]

    vals = pd.crosstab(index=df[var1],
                       columns=df[var2],
                       values=df[value],
                       aggfunc=aggfunc)
    if colors_by != "all":
        axis = 1 if colors_by == "rows" else 0
        vals_normalized = (vals.sub(vals.mean(axis=axis),
                                    axis=0 if axis == 1 else 1)
                               .div(vals.std(axis=axis),
                                    axis=0 if axis == 1 else 1))

    if pct_by:
        pct = pd.crosstab(index=df[var1],
                          columns=df[var2],
                          values=df[value],
                          aggfunc=aggfunc,
                          normalize="index" if pct_by == "rows" else "columns")
        if colors_by != "all":
            axis = 1 if colors_by == "rows" else 0
            pct_normalized = (pct.sub(pct.mean(axis=axis),
                                      axis=0 if axis == 1 else 1)
                                 .div(pct.std(axis=axis),
                                      axis=0 if axis == 1 else 1))

    fig, axs = plt.subplots(ncols=2 if pct_by else 1)

    if pct_by and cumulative_pct:
        pct = pct.expanding().sum()
        pct_normalized = pct_normalized.expanding().sum()

    # reverse rows
    if reverse is True or reverse == "rows":
        if colors_by != "all":
            vals_normalized = vals_normalized.iloc[::-1]
        vals = vals.iloc[::-1]
        if pct_by:
            if colors_by != "all":
                pct_normalized = pct_normalized.iloc[::-1]
            pct = pct.iloc[::-1]
    # reverse columns
    if reverse is True or reverse == "columns":
        if colors_by != "all":
            vals_normalized = vals_normalized.iloc[:, ::-1]
        vals = vals.iloc[:, ::-1]
        if pct_by:
            if colors_by != "all":
                pct_normalized = pct_normalized.iloc[:, ::-1]
            pct = pct.iloc[:, ::-1]
    try:
        ax1 = axs[0]
    except: # when there's only one, it's not a collection
        ax1 = axs

    if "fmt" in heatmap_kwargs:
        fmt = heatmap_kwargs.pop("fmt")
    else:
        fmt = ".0f"

    sns.heatmap(vals_normalized if colors_by != "all" else vals,
                ax=ax1,
                annot=vals,
                fmt=fmt,
                cbar=False,
                cmap=sequential_palette,
                **heatmap_kwargs)

    if pct_by:
        sns.heatmap(pct_normalized if colors_by != "all" else pct,
                    ax=axs[1],
                    annot=pct,
                    fmt=".1%",
                    cbar=False,
                    cmap=sequential_palette,
                    **heatmap_kwargs)

    ax1.set_title(title)
    if pct_by and pct_title:
        axs[1].set_title(pct_title)


def get_acad_year(term_cd, summer_start=False):
    if pd.isna(term_cd):
        return np.NaN
    parts = str(int(term_cd))
    year = ""
    if parts[0] == "1":
        year += "19"
    else:
        year += "20"
    year += parts[1:3]
    year = int(year)
    if parts[-1] == "7":
        year += 1
    if summer_start and parts[-1] == "4":
        year += 1
    return year


def get_expected_grad_term(start_term, years):
    return start_term + (years * 10) - (4 if str(start_term)[-1] == "1" else 3)
    

def get_fiscal_year(term_cd):
    if pd.isna(term_cd):
        return np.NaN
    parts = str(int(term_cd))
    if parts[0] == "2":
        year = 2000
    else:
        year = 1900
    year += int(parts[1:3])
    if parts[-1] != "1":
        year += 1
    return year


def get_student_outcome(x, years_to_grad):
    status = x["prog_stat_cd"]
    exp = get_expected_grad_term(x["first_term_cd"], years_to_grad)
    last_term = x["prog_comp_term_cd"]

    if status == "CM" and last_term <= exp:
        return "grad"
    elif status == "CM":
        return "ip" # in progress
    elif status == "DC":
        return "drop"
    else:
        return status


def get_term_season(term_cd):
    szn = int(str(term_cd)[-1])
    if szn in [7, 8]:
        return "Fall"
    if szn in [9, 0]:
        return "Winter"
    if szn in [1, 2, 3]:
        return "Spring"
    if szn in [4, 5, 6]:
        return "Summer"


def get_term_num_since_start(start_term: int, term: int) -> int:
    """
    Calculate number of terms from the start term to the current term.

    Notes:
    Per what I observed in the IR data, this automatically changes summer starts to fall.
    """
    # first change summer starts to fall, because that's what IR does
    if start_term % 2 == 0:
        start_term += 3
    diff = term - start_term
    years = diff // 10
    r = diff % 10
    terms = years * 3
    terms += 1

    if r > 4:
        terms += 2
    elif r > 0:
        terms += 1
    return int(terms)


def get_time_to_grad(first_term_cd: int, prog_comp_term_cd: int) -> float:
    """
    Calculate number of years from first term to graduation term.
    Each term counts as a third of a year.

    Notes: Per get_term_num_since_start, automatically changes summer starts to fall.
    """
    terms = get_term_num_since_start(first_term_cd, prog_comp_term_cd)
    years = np.round(terms / 3, 2)
    return years


def get_bin_labels(bin, integers_only=True, zero_is_min=True):
    if bin.closed == "both":
        raise Exception("get_bin_labels cannot handle double-closed intervals yet")
    right = bin.right
    left = bin.left
    if integers_only:
        try:
            right = int(right)
        except:
            pass
        try:
            left = int(left)
        except:
            pass
        if bin.closed == "right" and left != -np.Inf and right != np.Inf:
            left += 1
        elif bin.closed == "left" and left != -np.Inf and right != np.Inf:
            right += 1

    if left == -np.Inf:
        if bin.closed == "right" and zero_is_min:
            return str(right)
        elif bin.closed == "right":
            return f"{right} or less"
        else:
            return f"Less than {right}"
    elif right == np.Inf:
        if bin.closed == "right":
            return f"Greater than {left}"
        else:
            return f"{left} or greater"
    elif bin.length == 1 and integers_only:
        if bin.closed == "right":
            return str(right)
        else:
            return str(left)
    else:
        return f"{left} to {right}"


def get_model_name(model):
    """ Generates a human-readable string representing the model and some
        of its parameters in order to make it unique from others. Currently
        the only parameters are criterion, solver, and scale_pos_weight (bool).
    """

    model_name = str(model.__class__).split("'")[1].split(".")[-1]

    try:
        if isinstance(model.criterion, str):
            crit = model.criterion
        else:
            crit = str(model.criterion.__class__).split("'")[1].split(".")[-1]
        model_name += " - " + crit
    except:
        pass

    try:
        model_name += " - " + model.solver
    except:
        pass

    try:
        if model.scale_pos_weight != 1:
            model_name += " - Weighted"
    except:
        pass

    return model_name


def get_feature_importances(model, fields):
    """ Returns pandas Series of the feature importances from a scikit-learn model. Looks for these values
        first in model.coef_ and if that does not exist then it tries model.feature_importances_.
    """

    importances = None
    try:
        importances = model.coef_[0]
    except:
        pass
    try:
        importances = model.feature_importances_
    except:
        pass
    importances = pd.Series(importances, index=fields)
    return importances


def draw_bias_variance(model, X_train, X_test, y_train, y_test, step=500):
    """ Draw a bias-variance plot with seaborn.
    """
    bv_data = []

    for data_size in range(step, len(X_train), step):
        X_train_curr = X_train.sample(n=data_size)
        y_train_curr = y_train.loc[X_train_curr.index]
        model.fit(X_train_curr, y_train_curr)
        train_error = 1 - model.score(X_train_curr, y_train_curr)
        test_error = 1 - model.score(X_test, y_test)
        bv_data.append((data_size, train_error, "train"))
        bv_data.append((data_size, test_error, "test"))

    df_bv = pd.DataFrame(bv_data, columns=["data_size", "error", "error_type"])
    sns.lineplot(x="data_size", y="error", hue="error_type", data=df_bv)


def draw_precision_recall(y_true, y_proba, step=0.02):
    """ Draw a precision-recall plot with seaborn.
    """
    pr_data = []
    for threshold in np.arange(0, 1, step):
        y_pred = y_proba > threshold
        metrics = get_metrics(y_true, y_pred)
        precision, recall = metrics[0], metrics[1]
        pr_data.append((precision, recall, threshold))
    df_pr = pd.DataFrame(pr_data, columns=["precision", "recall", "threshold"])
    sns.lineplot(x="recall", y="precision", data=df_pr)
